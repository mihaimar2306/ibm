import * as readline from 'readline';
import { Device } from './units/device';
import { DeviceParamCalculator } from './calculators/deviceParamCalculator';
import { LinkStation } from './units/linkStation';


// main app
class App {
  
  public linkStations: LinkStation[] = [{position: {x: 0, y: 0}, reach: 10}, {position: {x: 20, y: 20}, reach: 5}, {position: {x: 10, y: 0}, reach: 12}];
  private calculator: DeviceParamCalculator = new DeviceParamCalculator(); 

  /**
   * adds a new link station to the link station list
   * @param {LinkStation} ls - new link station object
   * @return {void} Nothing
   */
  public addLinkstation(ls: LinkStation): void {
    this.linkStations.push(ls);
  }

  /**
 * method exposed to the user to check the power and optimal link station of a given device
 * @param {Device} device - device to analyze
 * @return {void} Nothing
 */
  public printBestLinkStation(device: Device): void {
    this.calculator.calculateOptimalLinkStation(device, this.linkStations);
  }
}

// create interface to read and write to terminal
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})


rl.question('Please enter x coordinate for device ', (x) => {
  const xPosition = x;
  rl.question('Please enter y coordinate for device ', (y) => {
    const yPosition = y

    const app = new App();
    
    app.printBestLinkStation(new Device({x: +xPosition, y: +yPosition}));

    rl.close();
  })
})

rl.on("close", function() {
  console.log("\nBYE BYE !!!");
  process.exit(0);
});