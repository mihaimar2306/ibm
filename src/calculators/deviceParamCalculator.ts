import { Device } from '../units/device';
import { LinkStation } from '../units/linkStation';

// Device parameter calculator
export class DeviceParamCalculator {

  /**
 * calculates optimal link station of a device and its power
 * @param {Device} device - The device to be analyzed
 * @param {Array} linkStations - The array of link station available
 * @return {void} Nothing
 */
  calculateOptimalLinkStation(device: Device, linkStations: LinkStation[]): void {

    let power = 0;
    let bestLs = linkStations[0];

    for (const ls of linkStations) {

      // Calculate distance between device and current link station
      const relativePower = device.calculatePower(ls);
    
      if (relativePower > power) {
        power = relativePower;
        bestLs = ls;
      }

    }

    if (power > 0) {
      console.log(`\nBest link station for device with coordinates {x: ${device.position.x}, y: ${device.position.y}} is link station with coordinates x: {${bestLs.position.x}, y: ${bestLs.position.y}} with power ${power}`)
    } else {
      console.log(`\nNo link station within reach for point {x: ${device.position.x}, y: ${device.position.y}}`)
    }

  }

}