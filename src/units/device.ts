import { LinkStation } from './linkStation';
import { Position } from './position';

// Class representing device to be measured
export class Device {
  position: Position;

  /**
   * Create a device.
   * @param {Position} position - contains the x and y coordinates
   */
  constructor(position: Position) {
    this.position = position;
  }

  /**
   * Calculates the distance between a given link station and the device
   * @param {LinkStation} linkStation - linkstation and its position
   * @return {number} distance between a given link station and the device 
   */
  private calculateDistance(linkStation: LinkStation): number {
    return Math.sqrt(Math.pow(this.position.x - linkStation.position.x,2) + Math.pow(this.position.y - linkStation.position.y,2))
  }

  /**
   * Calculates the power against a given link station
   * @param {LinkStation} linkStation - linkstation and its position
   * @return {number} power against a given link station
   */
  public calculatePower(ls: LinkStation): number {
    const d = this.calculateDistance(ls);
    return d > ls.reach ? 0 : Math.pow(ls.reach - d, 2);
  }

}