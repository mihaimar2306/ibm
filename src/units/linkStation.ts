import { Position } from './position';

export interface LinkStation {
  position: Position,
  reach: number
}